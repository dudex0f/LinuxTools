#!/bin/bash

IPT_SPAMHAUSE_CHAIN_NAME=spamhause
IPT_SMTP_CHAIN_NAME=smtp
IPT_SMTP_WHITELIST=/etc/mail/iptables-white-list
IPT_CUSTOM_DROP_FILE=/etc/iptables/ips_to_drop

NET_IF_NAME=ens3

flush_chain() {
    iptables -F ${1}
}

is_chain() {
	CHAIN=${1:-"dummy"}
	iptables --list | grep -q  "Chain ${CHAIN}"
}


is_chain_referenced_from_input() {
	CHAIN=${1:-"dummy"}
	iptables --list | grep -q  "^${CHAIN}"
}

add_chain() {
	CHAIN=${1:-"dummy"}
	iptables -N "${CHAIN}"
}

add_drop_chain_to_input() {
	iptables -I INPUT 1 -s 0.0.0.0  -i ${NET_IF_NAME} -j "${IPT_SPAMHAUSE_CHAIN_NAME}"
}

save_iptables() {
	iptables-save > /etc/iptables/iptables.rules
}

add_ip_to_drop_chain() {
	ip="${1:-noaddr}"
	iptables -A ${IPT_SPAMHAUSE_CHAIN_NAME} -i ${NET_IF_NAME} -s $ip -j DROP
}

apply_spamhause_drop() {
	for ip in $(wget http://www.spamhaus.org/drop/drop.txt -O - | awk '! /^;/ {print $1}');
	do 
		add_ip_to_drop_chain $ip
	done
}

apply_smtp_drop() {
    for ip in $(sed -n '/AUTH command used when not advertised/s/^.*\[\([0-9\.]\+\)\].*$/\1/gp'  /var/log/exim/mainlog*  | sort -u)
    do 
        grep -qvws $ip "${IPT_SMTP_WHITELIST}" && \
		add_ip_to_drop_chain $ip
    done
}


apply_custom_ip() {
	for ip in $(grep -v "^#" $IPT_CUSTOM_DROP_FILE)
	do
		add_ip_to_drop_chain $ip
	done
}

fw_do_spamhause() {
    flush_chain ${IPT_SPAMHAUSE_CHAIN_NAME}
    apply_spamhause_drop
    apply_custom_ip
    save_iptables
}

fw_do_smtp() {
    flush_chain ${IPT_SMTP_CHAIN_NAME}
    apply_smtp_drop
    save_iptables
}

fw_do_all() {
    fw_do_spamhause
    fw_do_smtp
}

