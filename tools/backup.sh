#
# backup tool to run on eeepc05/LinuxArch
#

BACKUP_DEV=/dev/sdb1
BACKUP_TAR=$(uname -n)-$(date +%F-%H%M%S).tar.bz2
BACKUP_PATH=/backup

function exit_with_err_msg() {
    echo "ERROR: ${1}"
    echo "Exiting..."
    exit 100
}

mount "${BACKUP_DEV}" "${BACKUP_PATH}" > /dev/null 2>&1 || exit_with_err_msg "Failed to mount ${BACKUP_DEV} on ${BACKUP_PATH}"

pacman -Qtte > /tmp/pacman.list || exit_with_err_msg "Failed to list installed packages"

tar -cjf "${BACKUP_PATH}"/"${BACKUP_TAR}" /root /etc /opt/apps/mmasker/mmasker.sqlite /opt/apps/allegro/aukcje.db /var/db/sshguard/ /tmp/pacman.list > /dev/null 2>&1 || exit_with_err_msg "Failed to run tar command"

rm -rf /tmp/pacman.list > /dev/null 2>&1
find "${BACKUP_PATH}"/ -name "$(uname -n)"*bz2 -mtime +30 -delete > /dev/null 2>&1 || exit_with_err_msg "failed to remove older savesets" 

umount "${BACKUP_PATH}" > /dev/null 2>&1 || exit_with_err_msg "failed to umount ${BACKUP_PATH}"
